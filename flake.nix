{
  description = "test cargo flake";

  inputs = {
    #nixpkgs.url = "nixpkgs/release-20.09";
    mozillapkgs = {
      url = "github:mozilla/nixpkgs-mozilla";
      flake = false;
    };
    flake-utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nmattia/naersk";
  };

  outputs = { self, nixpkgs, flake-utils, mozillapkgs, naersk }: 
  flake-utils.lib.eachDefaultSystem (system: let
    # project name
    projname = "test-crate";

    pkgs = nixpkgs.legacyPackages."${system}";

    mozilla = pkgs.callPackage (mozillapkgs + "/package-set.nix") {};
    rust = (mozilla.rustChannelOf {
        date = "2020-12-24";
        channel = "nightly";
        sha256 = "sha256-ltQYo4AYmpD4dJV9WXDf0VSwj2cxilS1hrjTsj7Fcng=";
      }).rust.override {
        extensions = [ "rust-src" "rust-analysis" ];
      };

    naersk-lib = naersk.lib."${system}".override {
      cargo = rust;
      rustc = rust;
    };
  in rec {
    # `nix build`
    packages."${projname}" = naersk-lib.buildPackage {
      pname = "${projname}";
      root = ./.;
    };
    defaultPackage = packages."${projname}";

    # `nix run`
    apps."${projname}" = flake-utils.lib.mkApp {
      drv = packages."${projname}";
    };
    defaultApp = apps."${projname}";

    # `nix shell` / direnv-nix
    devShell = pkgs.mkShell {
      buildInputs = with pkgs; [ 
        rust-analyzer
        rust
      ];
    };
  });
}
