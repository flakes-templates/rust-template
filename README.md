# Your everyday Rust project setup using Nix Flakes

## What is this?

This repository is meant to be used as a basis for simple-ish nix-flakes-based rust projects.

The `flakes.nix` file defines a devShell target which when called using `nix shell` will drop you into an interactive shell with all the required dependencies.

## How to use

To use this roughly follow these steps:

1. Fork it
2. Clone it
3. ...
4. Profit

Dont forget to edit the `projname` variable in the file `flakes.nix` to be the same as the `package.name` value in `Cargo.toml`.


## Others repos used

- [nix-direnv](https://github.com/nix-community/nix-direnv)
- [naersk](https://github.com/nmattia/naersk)
- [the mozilla nixpkgs overlay collection](https://github.com/mozilla/nixpkgs-mozilla)

## More on nix and flakes

- [Nix flakes](https://nixos.wiki/wiki/Flakes)
- [Nix](https://www.nixos.org)
- [the nix flakers rfc](https://github.com/NixOS/rfcs/pull/49)
- [Part 1 of a series of blog posts on nix flakes](https://www.tweag.io/blog/2020-05-25-flakes/)
- [the nix flakes talk on youtube](https://www.youtube.com/watch?v=UeBX7Ide5a0)
